/**
 * Methods to practice recursive calls and computations.
 * 
 * @author Sami
 *
 */

public class Recursion {

    /**
     * A method to compute the power(n) of a given base number(base).
     * @param base the base number of the computation
     * @param n the nth power to compute the base to
     * @return the base number to the power of n
     */
    public static int powerN(int base, int n) {
        if (n <= 0) {
            return 1;
        } else {
            return base * powerN(base, n - 1);
        }
    }
    
    /**
     * A simple method for counting the number of blocks in a simple
     *     triangle made out of blocks. Each row number has the same amount
     *     of blocks as its number.
     * @param row the number of rows in the triangle
     * @return the number of blocks needed to create the triangle
     */
    public static int triangle(int row) {
        if (row <= 0) {
            return 0;
        } else if (row == 1) {
            return 1;
        } else {
            return row + triangle(row - 1);
        }
    }
}
